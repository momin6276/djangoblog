from django.urls import path,include
from .views import ProfileView,ChangePassword
from django.contrib.auth.decorators import login_required
urlpatterns = [
    path('profile/',login_required(ProfileView.as_view()),name="profile"),
    path('changepassword/',login_required(ChangePassword.as_view()),name="changepassword"),
]