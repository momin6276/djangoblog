from django import forms
from django.contrib.auth.models import User
from .models import Profile

class ProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):

        super(ProfileForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
        self.fields['fname'].widget.attrs['placeholder'] = 'Enter first Name'
        self.fields['lname'].widget.attrs['placeholder'] = "Enter last Name"
        # self.fields['fname'].widget.attrs['value'] = prof.fname 
        # self.fields['lname'].widget.attrs['value'] = prof.lname 
        # self.fields['dob'].initial =prof.dob 
        # self.fields['gender'].initial=prof.gender
        self.fields['profile_image'].widget.attrs['class'] = 'form-control rounded'

    class Meta:
        model=Profile
        fields=['dob','fname','lname','gender','profile_image']

        widgets = {
            'dob': forms.DateInput(attrs={'type': 'date'})
        }