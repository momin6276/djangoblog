from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.
class Profile(models.Model):
    gender_choice=[('Male','Male'),('Female','Female'),('Other','Other')]

    user=models.OneToOneField(User, on_delete=models.CASCADE)
    dob = models.DateField(default=timezone.now)
    fname = models.CharField(max_length=50,blank=True)
    lname = models.CharField(max_length=50,blank=True)
    gender = models.CharField(choices=gender_choice,max_length=6,default='Male')
    profile_image=models.ImageField()
    
    def __str__(self):
        return self.user.username
    