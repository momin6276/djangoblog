from django.shortcuts import render
from django.views import View
from .models import Profile
from .froms import ProfileForm
from posts.models import Category 
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User



Category=Category.objects.all()
# Create your views here.
class ProfileView(View):

    def get(self,request):
        profile=Profile.objects.get(user=request.user)
        my_form=ProfileForm(instance=profile)
        img=Profile.objects.get(user_id=request.user.id).profile_image

        return render(request, 'user/profile.html',{'profile':my_form,'category':Category,'img':img})

    
    def post(self,request):
        profile=Profile.objects.get(user=request.user)
        my_form=ProfileForm(request.POST,request.FILES,instance=profile)
        if my_form.is_valid():
            
            fname=my_form.cleaned_data['fname']
            lname=my_form.cleaned_data['lname']
            

            if fname=="":
                messages.info(request, 'First name shouldn\'t be empty')
            elif lname=="":
                messages.info(request, 'Last name shouldn\'t be empty')
            else:
                my_form.save()
                messages.info(request,"Your profile updated successfully")

        return HttpResponseRedirect('/profile')

class ChangePassword(View):

    def get(self,request):
        return render(request, 'user/changepassword.html',{'category':Category})

    def post(self,request):
        u=User.objects.get(id=request.user.id)
        newpass=request.POST['npass']
        confirmpass=request.POST['cpass']
        currentpass=request.POST['cupass']
        if len(newpass)<8:
            messages.info(request,"Password can't be smaller than 8 numbers")
        elif newpass!=confirmpass:
            messages.info(request,"New password and confirm password didn't match")
        elif not u.check_password(currentpass):
            messages.info(request,'Your currentpassword is wrong')
        else:
            
            u.set_password(newpass)
            u.save()
            messages.info(request,'Your password changed successfully')
        
        return render(request, 'user/changepassword.html',{'category':Category})

