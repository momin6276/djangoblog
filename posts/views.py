from django.shortcuts import render
from .models import Category,Posts
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View
from .forms import CreatePostForm
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User
# Create your views here.

catg=Category.objects.all()


def posts(request):
    posts=Posts.objects.all().order_by('-created_at')
    page_obj=pagin_ator(request,5,posts)
    return render(request, 'post/index.html',{'category':catg,'posts': page_obj})

def category(request,cat):
    posts=Posts.objects.filter(category__category=cat)
    page_obj=pagin_ator(request,5,posts)
    return render(request, 'post/index.html',{'category':catg,'posts': page_obj})

def post(request,pid):
    post=Posts.objects.get(pk=pid)
    related=Posts.objects.all().filter(category=post.category).exclude(pk=pid)
    return render(request,'post/single.html',{'category':catg,'post':post,'related':related})

def pagin_ator(request,num, data):
    paginator = Paginator(data, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return page_obj


class CreatePost(View):
    my_form=CreatePostForm

    @method_decorator(login_required(login_url='login'))
    def get(self,request):
        return render(request,'post/create_post.html',{'category':catg,'post':self.my_form})

    def post(self,request):
        form = self.my_form(request.POST or None, request.FILES or None)
        
        if form.is_valid():
            title=form.cleaned_data['title']
            post=form.cleaned_data['post']
            category=form.cleaned_data['category']
            image=form.cleaned_data['post_image']
            
            total_words=post.split()

            if title=="":
                messages.info(request, 'Title can\'t be empty')
            elif len(title)<20:
                messages.info(request, 'Title should be more than 20 characters')
            elif len(total_words)<10:
                messages.info(request, 'Not less than 10 words post is allowed')
            elif image=="":
                messages.info(request, "You must select a image")
            else:
                user = User.objects.get(id=request.user.id)

                new_post=Posts(title=title,post=post,category=category,post_image=image,posted_by=user)
                new_post.save()
                form = self.my_form()
                return HttpResponseRedirect("/")
        
        return render(request,'post/create_post.html',{'category':catg,'post':form})
        