from django.urls import path,include
from django.contrib.auth.decorators import login_required
from . import views
urlpatterns = [
    path('', views.posts,name='posts'),
    path('category/<str:cat>/',views.category,name='category'),
    path('post/<int:pid>/',views.post,name='post'),
    path('create_post',views.CreatePost.as_view(),name='post'),
]