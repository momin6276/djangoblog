from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
    category = models.CharField(blank=False, max_length=50,unique=True)

    def __str__(self):
        return self.category
    
class Posts(models.Model):
    title = models.CharField(max_length=200,blank=False)
    post = models.TextField(blank=False)
    posted_by=models.ForeignKey(User, on_delete=models.CASCADE)
    category=models.ForeignKey(Category,on_delete=models.CASCADE,null=False)
    post_image=models.ImageField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.title
    
