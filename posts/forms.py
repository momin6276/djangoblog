from django.forms import ModelForm
from .models import Posts

class CreatePostForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(CreatePostForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            self.fields['title'].widget.attrs['placeholder'] = 'Enter title(not less than 20 characters)'
            self.fields['post'].widget.attrs['placeholder'] = 'Enter new post(at least 10 words)'
            self.fields['category'].widget.attrs['placeholder'] ='Category'

            

    class Meta:
        model=Posts
        fields=['title','post','category','post_image']