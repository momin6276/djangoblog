from django import forms
from django.contrib.auth.models import User

class RegisterForm(forms.ModelForm):
    password2=forms.CharField(widget=forms.PasswordInput)


    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            self.fields['username'].widget.attrs['placeholder'] = self.fields['username'].label
            self.fields['email'].widget.attrs['placeholder'] = self.fields['email'].label
            self.fields['password'].widget.attrs['placeholder'] = self.fields['password'].label
            self.fields['password2'].widget.attrs['placeholder'] ='Confirm password'


    class Meta:
        model= User
        fields=('username','email','password','password2')


class LoginForm(forms.Form):
    username = forms.CharField(required=True,widget=forms.TextInput(attrs={'placeholder': 'Username','class':'form-control'}))
    password = forms.CharField( required=True,widget = forms.PasswordInput(attrs={'placeholder': 'password','class':'form-control'}))