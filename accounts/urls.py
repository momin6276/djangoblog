from django.urls import path,include
from .views import Register,Login,logout_view
urlpatterns = [
    path('register', Register.as_view(),name="register"),
    path('login', Login.as_view(),name="login"),
    path('logout',logout_view,name='logout'),
]