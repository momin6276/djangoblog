from django.shortcuts import render
from django.views import View
from .forms import RegisterForm,LoginForm
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
# Create your views here.

class Register(View):
    my_form=RegisterForm

    def get(self,request):
        if request.user.is_authenticated:
            return HttpResponseRedirect("/")

        return render(request, 'account/register.html',{'register':self.my_form})


    def post(self,request,*args, **kwargs):
        form = self.my_form(request.POST)

        if form.is_valid():
            uname=form.cleaned_data['username']
            email=form.cleaned_data['email']
            password=form.cleaned_data['password']
            confirm_password=form.cleaned_data['password2']      

            if User.objects.filter(email=email).exists():
                messages.info(request, 'You already have an account')

            elif len(password)<8:
                messages.info(request, 'Password length should be 8 or more than 8')

            elif password != confirm_password:
                messages.info(request, 'Password and confirm password didn\'t match')

            else:
                user=User.objects.create_user(username=uname,password=password,email=email)
                user.save()
                return HttpResponseRedirect('/login')
        else:
          messages.info(request, 'Username is already taken')


        return render(request, 'account/register.html',{'register':self.my_form})


class Login(View):
    my_form=LoginForm

    def get(self,request):
        if request.user.is_authenticated:
            return HttpResponseRedirect("/")

        return render(request, 'account/login.html',{'login':self.my_form})


    def post(self,request):
        form = self.my_form(request.POST)
        

        if form.is_valid():
            uname=form.cleaned_data['username']
            password=form.cleaned_data['password']

            user=authenticate(username=uname, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect("/")
            else:
                messages.info(request, 'Invalid Username Or Password')
        

        return render(request, 'account/login.html',{'login':self.my_form})



login_required(login_url='login')
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')